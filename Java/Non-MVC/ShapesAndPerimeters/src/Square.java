/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author crbxi
 */
public class Square extends Shape {
    double length;

    public Square(double length) {
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
    
    
    
    @Override
    public double area() {
        return Math.pow(length, 2);
    }
    @Override
    public double perimeter() {
        return 4*length;
    }
}
