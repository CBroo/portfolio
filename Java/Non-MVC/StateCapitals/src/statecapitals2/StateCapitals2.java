/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package statecapitals2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author crbxi
 */
public class StateCapitals2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int minPop;
        Map<String, Capital> capitals = new HashMap<>();
        
        //Create capital objects for every state
        Capital alabama = new Capital("Montgomery", 200983, 155.4);
        Capital alaska = new Capital("Juneau", 31118, 2716.7);
        Capital arizona = new Capital("Phoenix", 1418041, 474.9);
        Capital arkansas= new Capital("Little Rock", 184081, 116.2);
        Capital california = new Capital("Sacramento", 454330, 97.2);
        Capital colorado = new Capital("Denver", 556835, 153.4);
        Capital connecticut = new Capital("Hartford", 124848, 17.3);
        Capital delaware = new Capital("Dover", 32135, 22.4);
        Capital florida = new Capital("Tallahassee", 156612, 95.7);
        Capital georgia = new Capital("Atlanta", 419122, 131.7);
        Capital hawaii = new Capital("Honolulu", 377260, 85.7);
        Capital idaho = new Capital("Boise", 190122, 63.8);
        Capital illinois = new Capital("Springfield", 114738, 54.0);
        Capital indiana = new Capital("Indianapolis", 784242, 361.5);
        Capital iowa = new Capital("Des Moines", 194311, 75.8);
        Capital kansas = new Capital("Topeka", 122377, 56.0);
        Capital kentucky = new Capital("Frankfort", 27741, 14.7);
        Capital louisiana = new Capital("Baton Rouge", 224097, 76.8);
        Capital maine = new Capital("Augusta", 18560, 55.4);
        Capital maryland = new Capital("Annapolis", 36217, 6.73);
        Capital massachusetts = new Capital("Boston", 569165, 48.4);
        Capital michigan = new Capital("Lansing", 116941, 35.0);
        Capital minnesota = new Capital("Saint Paul", 276963, 52.8);
        Capital mississippi = new Capital("Jackson", 179298, 104.9);
        Capital missouri = new Capital("Jefferson City", 39636, 27.3);
        Capital montana = new Capital("Helena", 25780, 14.0);
        Capital nebraska = new Capital("Lincoln", 236146, 74.6);
        Capital nevada = new Capital("Carson City", 52457, 143.4);
        Capital newHampshire = new Capital("Concord", 40687, 64.3);
        Capital newJersey = new Capital("Trenton", 85402, 7.66);
        Capital newMexico = new Capital("Santa Fe", 62203, 37.3);
        Capital newYork = new Capital("Albany", 95658, 21.4);
        Capital northCarolina = new Capital("Raleigh", 326653, 114.6);
        Capital northDakota = new Capital("Bismarck", 55532, 26.9);
        Capital ohio = new Capital("Columbus", 730008, 210.3);
        Capital oklahoma = new Capital("Oklahoma City", 528042, 607.0);
        Capital oregon = new Capital("Salem", 146120, 45.7);
        Capital pennsylvania = new Capital("Harrisburg", 48950, 8.11);
        Capital rhodeIsland = new Capital("Providence", 178126, 18.5);
        Capital southCarolina = new Capital("Columbia", 116331, 125.2);
        Capital southDakota = new Capital("Pierre", 13876, 13.0);
        Capital tennessee = new Capital("Nashville", 546719, 473.3);
        Capital texas = new Capital("Austin", 681804, 251.5);
        Capital utah = new Capital("Salt Lake City", 178605, 109.1);
        Capital vermont = new Capital("Montpelier", 8035, 10.2);
        Capital virginia = new Capital("Richmond", 192494, 60.1);
        Capital washington = new Capital("Olympia", 42514, 16.7);
        Capital westVirginia = new Capital("Charleston", 53421, 31.6);
        Capital wisconsin = new Capital("Madison", 220332, 68.7);
        Capital wyoming = new Capital("Cheyenne", 55362, 21.1);
        
        //add states and capital objects to hashmap
        capitals.put("Alabama", alabama);
        capitals.put("Alaska",alaska);
        capitals.put("Arizona",arizona);
        capitals.put("Arkansas",arkansas);
        capitals.put("California",california);
        capitals.put("Colorado",colorado);
        capitals.put("Connecticut",connecticut);
        capitals.put("Delaware",delaware);
        capitals.put("Florida",florida);
        capitals.put("Georgia",georgia);
        capitals.put("Hawaii",hawaii);
        capitals.put("Idaho",idaho);
        capitals.put("Illinois",illinois);
        capitals.put("Indiana",indiana);
        capitals.put("Iowa",iowa);
        capitals.put("Kansas",kansas);
        capitals.put("Kentucky",kentucky);
        capitals.put("Louisiana",louisiana);
        capitals.put("Maine",maine);
        capitals.put("Maryland",maryland);
        capitals.put("Massachusetts",massachusetts);
        capitals.put("Michigan",michigan);
        capitals.put("Minnesota",minnesota);
        capitals.put("Mississippi",mississippi);
        capitals.put("Missouri",missouri);
        capitals.put("Montana",montana);
        capitals.put("Nebraska",nebraska);
        capitals.put("Nevada",nevada);
        capitals.put("New Hampshire",newHampshire);
        capitals.put("New Jersey",newJersey);
        capitals.put("New Mexico",newMexico);
        capitals.put("New York",newYork);
        capitals.put("North Carolina",northCarolina);
        capitals.put("North Dakota",northDakota);
        capitals.put("Ohio",ohio);
        capitals.put("Oklahoma",oklahoma);
        capitals.put("Oregon",oregon);
        capitals.put("Pennsylvania",pennsylvania);
        capitals.put("Rhode Island",rhodeIsland);
        capitals.put("South Carolina",southCarolina);
        capitals.put("South Dakota",southDakota);
        capitals.put("Tennessee",tennessee);
        capitals.put("Texas",texas);
        capitals.put("Utah",utah);
        capitals.put("Vermont",vermont);
        capitals.put("Virginia",virginia);
        capitals.put("Washington",washington);
        capitals.put("West Virginia",westVirginia);
        capitals.put("Wisconsin",wisconsin);
        capitals.put("Wyoming",wyoming);
        
        Set<String> keys = capitals.keySet();

        //print states with their capitals and info
        System.out.println("States And Capitals and Stats:");
        for(String k : keys) {
            System.out.println(k + " -- " + capitals.get(k).getName() + " - " + capitals.get(k).getPopulation() + " - " + capitals.get(k).getMileage());
        }
        
        System.out.println("What is the minimum population you want to see?");
        minPop = scanner.nextInt();
        for(String k : keys) {
            if(capitals.get(k).getPopulation()>minPop) {
                System.out.println(k + " -- " + capitals.get(k).getName() + " - " + capitals.get(k).getPopulation() + " - " + capitals.get(k).getMileage());
            }
        }
    }
    
}
