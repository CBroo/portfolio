$(document).ready(function() {
	$.ajax({
		url: "http://localhost:8080/items",
		type: 'GET',
		success: function(data) {
			$.each(data, function(i,item){
				var inventoryDiv = $("#inventory");
				var itemButton = "<button class='btn btn-info inv-btn' id='"+item.id+"'>";
				itemButton +="<div class='col-sm-2'>";
				itemButton+="<p>";
				itemButton+=item.id+".";
				itemButton+="</p>";
				itemButton+="</div>";
				itemButton+="<div class='col-sm-4'>";
				itemButton+="<p class='productName'>";
				itemButton+=item.name;
				itemButton+="</p>";
				itemButton+="<p class='productPrice'>";
				itemButton+=parseFloat(item.price).toFixed(2);
				itemButton+="</p>";
				itemButton+="<p class='productInv'>";
				itemButton+="Quantity Left: " + item.quantity;
				itemButton+="</p>";
				itemButton+="</div>";
				itemButton+="</button>";

				inventoryDiv.append(itemButton);
			})
		},
		error: function() {
			alert("failure");
		}
	});

	var quarters, dimes, nickels, pennies;
	var total = 0.00;
	$("#dollar").on("click", function () {
		total = Number(total) + 1.00;
		$("#money_in").attr("placeholder", parseFloat(total).toFixed(2));
	});
	$("#quarter").on("click", function () {
		total = Number(total) + .25;
		$("#money_in").attr("placeholder", parseFloat(total).toFixed(2));
	});
	$("#dime").on("click", function () {
		total = Number(total) + .1;
		$("#money_in").attr("placeholder", parseFloat(total).toFixed(2));
	});
	$("#nickel").on("click", function () {
		total = Number(total) + .05;
		$("#money_in").attr("placeholder", parseFloat(total).toFixed(2));
	});
	$("#money_in").attr("placeholder", parseFloat(total).toFixed(2));

	$("#inventory").on("click", "button", function() {
		$("#item_id").attr("placeholder", $(this).attr('id'));
	});

	$("#purchase").on("click", function() {
		$.ajax({
			url: "http://localhost:8080/money/"+$("#money_in").attr("placeholder")+"/item/"+$("#item_id").attr("placeholder"),
			type: 'GET',
			success: function(data) {
				var itemId=$("#item_id").attr("placeholder");
				var itemPrice=$("#"+itemId+" .productPrice").text();
				var currentMoney=$("#money_in").attr("placeholder");
				var newMoney=currentMoney-itemPrice;
				$("#money_in").attr("placeholder", parseFloat(newMoney).toFixed(2));
				var change = "";
				$.each(data, function(type, num) {
					if(num>0) {
						change+=num+" "+type+" ";
					}

					$("#change").attr("placeholder", change);
					$("#messages").attr("placeholder", "Thank you!");
				})

				$.ajax({
					url: "http://localhost:8080/items",
					type: 'GET',
					success: function(data) {
						$.each(data, function(i,item){
							$("#"+item.id+" .productName").text(item.name);
							$("#"+item.id+" .productPrice").text(parseFloat(item.price).toFixed(2));
							$("#"+item.id+" .productInv").text("Quantity Left: "+item.quantity);
						});
					},
					error: function() {
						alert("failure");
					}
				});
			},
			error: function(data) {
				var jObj = $.parseJSON(data.responseText);
				$("#messages").attr("placeholder",jObj.message);
				
			}
		});
	});

	$("#change_return").on("click", function() {
		$("#money_in").attr("placeholder", "0.00");
		$("#item_id").attr("placeholder", "");
		$("#messages").attr("placeholder", "Good Bye!!");
		$("#change").attr("placeholder", "");
	});
});
