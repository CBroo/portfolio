$(document).ready(function() {
	var search_term;
	var search_category;
	var dvd_id;
	refreshList();

	$("#search").on("click", function() {
		search_term = $("#search_term").val();
		search_category = $("#search_category").find(":selected").val();
		if(search_category==="category" || !search_term) {
			$("#search_alert").show();
			return false;
		};
		$("#searchModal").modal();
		$.ajax({
			url: "http://localhost:8080/dvds/"+search_category+"/"+search_term,
			type: 'GET',
			success: function(data) {
				var dvdList;
				$.each(data, function(i, dvd) {
					$("#search_title").text(dvd.title);
					$("#searchList").empty();
					dvdList="<ul>";
					dvdList+="<li>Release Year: "+dvd.realeaseYear+"</li>";
					dvdList+="<li>Directior: "+dvd.director+"</li>";
					dvdList+="<li>Rating: "+dvd.rating+"</li>";
					dvdList+="<li>Notes: "+dvd.notes+"</li>";
					dvdList+="</ul>";
					$("#searchList").append(dvdList);
					$("#search_alert").hide();
				});
			},
			error: function() {
				alert("failure");
			}
		});
	});

	$("#dvdListBody").on("click", ".edit", function() {
		dvd_id=$(this).attr("value");
		$.ajax({
			url: "http://localhost:8080/dvd/"+dvd_id,
			type: 'GET',
			success: function(data) {
				$("#dvd_edit_title").val(data.title);
				$("#dvd_edit_year").val(data.realeaseYear);
				$("#dvd_edit_director").val(data.director);
				$("#dvd_edit_rating").val(data.rating);
				$("#dvd_edit_notes").val(data.notes);
			},
			error: function() {
				alert("failure");
			}
		});
		$("#editModal").modal();
	});

	$("#editIt").on("click", function() {
		

		$.ajax({
			url: "http://localhost:8080/dvd/"+dvd_id,
			type: 'PUT',
			success: function() {
				refreshList();
			},
			error: function() {
				alert("failure");
			}
		});
	});

	$("#dvdListBody").on("click", ".delete", function() {
		dvd_id=$(this).attr("id");
		$("#deleteModal").modal();
	});

	$("#deleteIt").on("click", function() {
		$.ajax({
			url: "http://localhost:8080/dvd/"+dvd_id, 
			type: 'DELETE',
			success: function() {
				refreshList();
			},
			error: function() {
				alert("failure");
			}
		});
	});

	$("#create").on("click", function() {
		$("#createModal").modal();
	});

	$("#createIt").on("click", function() {
		if(!$("#dvd_title").val()) {
			$("#title_alert").show();
			return false;
		};
		$("#title_alert").hide();
		var regex = new RegExp(/^[0-9]{4}$/);
		if(!(regex.test($("#dvd_year").val()))) {
			$("#year_alert").show();
			return false;
		};
		$("#year_alert").hide();
		$.ajax({
			url: "http://localhost:8080/dvd",
			type: 'POST',
			data: JSON.stringify({
				title: $("#dvd_title").val(),
				realeaseYear: $("#dvd_year").val(),
				director: $("#dvd_director").val(),
				rating: $("#dvd_rating").find(":selected").val(),
				notes: $("#dvd_notes").val()
			}),
			headers: {
        		"Accept": "application/json",
        		"Content-Type": "application/json"
   			},
			success: function(data) {
				$("#dvd_title").val("");
				$("#dvd_year").val("");
				$("#dvd_director").val("");
				$("#dvd_rating").val("G");
				$("#dvd_notes").val("");
				refreshList();
			},
			error: function() {
				alert("failure");
			}
		});
	});

	$("#search_alert").hide();
	$("#title_alert").hide();
	$("#year_alert").hide();
});

function refreshList() {

	$.ajax({
		url: "http://localhost:8080/dvds",
		type: 'GET',
		success: function(data) {
			var dvdTable;
			$("#dvdListBody").empty();
			$.each(data, function(i, dvd) {
				dvdTable="<tr>";
				dvdTable+="<td>"+dvd.title+"</td>";
				dvdTable+="<td>"+dvd.realeaseYear+"</td>";
				dvdTable+="<td>"+dvd.director+"</td>";
				dvdTable+="<td>"+dvd.rating+"</td>";
				dvdTable+="<td><a class='edit' value="+dvd.dvdId+" href='javascript:void(0)'>Edit</a> | <a class='delete' id="+dvd.dvdId+" href='javascript:void(0)'>Delete</a></td>";
				
				$("#dvdListBody").append(dvdTable);
			});
		},
		error: function() {
			alert("failure");
		}
	});
};