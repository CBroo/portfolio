/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userio;

import java.util.Scanner;

/**
 *
 * @author crbxi
 */
public class App {
    
    public static void main(String[] args) {
        SimpleCalculator calculator = new SimpleCalculator();
        int choice;
        double numOne;
        double numTwo;
        double result = 0;
        getInput input = new getInput();
        String menu = "***Menu***\n(1)Add\n(2)Subtract\n(3)Multiply\n(4)Divide\n(0)Exit";
        String getNums = "What number would you like to use?";
        
        while(true) {//loop until user chooses to exit
            choice = input.readInt(menu, 0, 4);
            
            if (choice == 0) {
                System.exit(0);
            }
            
            numOne = input.readDouble(getNums);
            numTwo = input.readDouble(getNums);
            //match choice to SimpleCalculator operation
            if(choice == 1) {
                result = calculator.add(numOne, numTwo);
            } else if (choice == 2) {
                result = calculator.subtract(numOne, numTwo);
            } else if (choice == 3) {
                result = calculator.multiply(numOne, numTwo);
            } else if (choice == 4){
                result = calculator.divide(numOne, numTwo);
            }
            System.out.println("The result is: " + result);
        }
    }
}
