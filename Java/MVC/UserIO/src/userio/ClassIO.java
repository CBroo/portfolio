/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userio;

import java.util.Scanner;

/**
 *
 * @author crbxi
 */
public class ClassIO implements UserIO {
    private Scanner scanner = new Scanner(System.in);
    @Override
    public void print(String msg) {
        System.out.println(msg);
    }

    @Override
    public double readDouble(String prompt) {
        System.out.println(prompt);
        while(!scanner.hasNextDouble()) {
            System.out.println(prompt);
            scanner.next();
        }
        double input = scanner.nextDouble();
        scanner.nextLine();
        return input;
    }

    @Override
    public double readDouble(String prompt, double min, double max) {
        double input;
        do {
            System.out.println(prompt);
            while(!scanner.hasNextDouble()) {
                System.out.println(prompt);
                scanner.next();
            }
            input = scanner.nextDouble();
            scanner.nextLine();
        } while(input<min || input>max);
        return input;
    }

    @Override
    public float readFloat(String prompt) {
        System.out.println(prompt);
        while(!scanner.hasNextFloat()) {
            System.out.println(prompt);
            scanner.next();
        }
        float input = scanner.nextFloat();
        scanner.nextLine();
        return input;
    }

    @Override
    public float readFloat(String prompt, float min, float max) {
        float input;
        do {
            System.out.println(prompt);
            while(!scanner.hasNextFloat()) {
                System.out.println(prompt);
                scanner.next();
            }
            input = scanner.nextFloat();
            scanner.nextLine();
        } while(input<min || input>max);
        return input;
    }

    @Override
    public int readInt(String prompt) {
        System.out.println(prompt);
        while(!scanner.hasNextInt()) {
            System.out.println(prompt);
            scanner.next();
        }
        int input = scanner.nextInt();
        scanner.nextLine();
        return input;
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        
        int input;
        do {
            System.out.println(prompt);
            while(!scanner.hasNextInt()) {
                System.out.println(prompt);
                scanner.next();
            }
            input = scanner.nextInt();
            scanner.nextLine();
        } while(input<min || input>max);
        return input;
    }

    @Override
    public long readLong(String prompt) {
        System.out.println(prompt);
        while(!scanner.hasNextLong()) {
            System.out.println(prompt);
            scanner.next();
        }
        long input = scanner.nextLong();
        scanner.nextLine();
        return input;
    }

    @Override
    public long readLong(String prompt, long min, long max) {
        long input;
        do {
            System.out.println(prompt);
            while(!scanner.hasNextLong()) {
                System.out.println(prompt);
                scanner.next();
            }
            input = scanner.nextLong();
            scanner.nextLine();
        } while(input<min || input>max);
        return input;
    }

    @Override
    public String readString(String prompt) {
        System.out.println(prompt);
        String input = scanner.nextLine();
        return input;
    }
    
}
