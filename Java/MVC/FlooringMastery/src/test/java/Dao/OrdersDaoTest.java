/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.Order;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author crbxi
 */
public class OrdersDaoTest {
    private OrderDao dao = new OrderDaoFileImpl();
    
    public OrdersDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createOrder method, of class OrderDao.
     */
    @Test
    public void testCreateOrder() throws Exception {
        Order order = new Order();
        order.setCustomerName("Brooks");
        order.setProductType("Carpet");
        order.setState("OH");
        order.setArea(new BigDecimal("100"));
        
        dao.createOrder(order);
        LocalDate today = LocalDate.now();

        List<Order> fromDao = dao.findAllOrdersOnDate(today);
        assertEquals(order.getCustomerName(), fromDao.get(0).getCustomerName());
        assertEquals(order.getProductType(), fromDao.get(0).getProductType());
        assertEquals(order.getState(), fromDao.get(0).getState());
        assertEquals(order.getArea(), fromDao.get(0).getArea());
    }

    /**
     * Test of removeOrder method, of class OrderDao.
     */
    @Test
    public void testRemoveOrder() throws Exception {
        Order order = new Order();
        order.setCustomerName("Brooks");
        order.setProductType("Carpet");
        order.setState("OH");
        order.setArea(new BigDecimal("100"));
        
        dao.createOrder(order);
        LocalDate today = LocalDate.now();
        dao.removeOrder(today, 1);
        boolean thrown = false;
        try{
            dao.findOrder(today,1);
        } catch (FlooringDaoException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }

    /**
     * Test of editOrder method, of class OrderDao.
     */
    @Test
    public void testEditOrder() throws Exception {
        Order order1 = new Order();
        order1.setCustomerName("Brooks");
        order1.setProductType("Carpet");
        order1.setState("OH");
        order1.setArea(new BigDecimal("100"));
        
        dao.createOrder(order1);
        LocalDate today = LocalDate.now();
        
        Order order2 = dao.findOrder(today, 1);
        order2.setCustomerName("John");
        order2.setProductType("Laminate");
        order2.setState("IN");
        order2.setArea(new BigDecimal("50"));
        
        dao.editOrder(today, order2);
        
        Order fromDao = dao.findOrder(today, 1);
        assertEquals(fromDao.getCustomerName(), order2.getCustomerName());
        assertEquals(fromDao.getProductType(), order2.getProductType());
        assertEquals(fromDao.getState(), order2.getState());
        assertEquals(fromDao.getArea(), order2.getArea());
    }

    /**
     * Test of findAllOrdersOnDate method, of class OrderDao.
     */
    @Test
    public void testFindAllOrdersOnDate() throws Exception {
        List<Order> orderList = new ArrayList<>();
        Order order1 = new Order();
        order1.setCustomerName("Brooks");
        order1.setProductType("Carpet");
        order1.setState("OH");
        order1.setArea(new BigDecimal("100"));
        
        dao.createOrder(order1);
        orderList.add(order1);
        LocalDate today = LocalDate.now();
        
        Order order2 = new Order();
        order2.setCustomerName("John");
        order2.setProductType("Laminate");
        order2.setState("IN");
        order2.setArea(new BigDecimal("50"));
        
        dao.createOrder(order2);
        orderList.add(order2);
        
        List<Order> savedOrderList = dao.findAllOrdersOnDate(today);
        assertEquals(orderList, savedOrderList);
    }

    /**
     * Test of findOrder method, of class OrderDao.
     */
    @Test
    public void testFindOrder() throws Exception {
        Order order = new Order();
        order.setCustomerName("Brooks");
        order.setProductType("Carpet");
        order.setState("OH");
        order.setArea(new BigDecimal("100"));
        
        dao.createOrder(order);
        LocalDate today = LocalDate.now();
        
        Order fromDao = dao.findOrder(today, 1);
        assertEquals(order, fromDao);
    }
    
}
