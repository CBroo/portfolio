/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.Taxes;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author crbxi
 */
public class TaxesDaoTest {
    
    private TaxesDao dao = new TaxesDaoFileImpl();
    
    public TaxesDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getTax method, of class TaxesDao.
     */
    @Test
    public void testGetTax() throws Exception {
        Taxes tax = dao.getTax("OH");
        Double expected = 6.25;
        
        assertEquals(tax.getState(), "OH");
        assertEquals(tax.getTax(), expected);
    }    
}
