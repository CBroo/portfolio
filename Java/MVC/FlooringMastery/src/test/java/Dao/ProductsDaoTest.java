/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.Product;
import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author crbxi
 */
public class ProductsDaoTest {
    private ProductDao dao = new ProductDaoFileImpl();
    
    public ProductsDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getProduct method, of class ProductDao.
     */
    @Test
    public void testGetProduct() throws Exception {
        Product product = dao.getProduct("Carpet");
        
        assertEquals(product.getProduct(), "Carpet");
        assertEquals(product.getCostPerSqFt(), new BigDecimal(2.25));
        assertEquals(product.getLaborCostPerSqFt(), new BigDecimal(2.10).setScale(2, BigDecimal.ROUND_HALF_UP));
    }
    
}
