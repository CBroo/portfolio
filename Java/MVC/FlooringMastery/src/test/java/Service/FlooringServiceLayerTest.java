/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Dto.Order;
import java.math.BigDecimal;
import java.time.LocalDate;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author crbxi
 */
public class FlooringServiceLayerTest {

    private FlooringServiceLayer service;
    
    public FlooringServiceLayerTest() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        service = ctx.getBean("serviceLayer", FlooringServiceLayer.class);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createOrder method, of class FlooringServiceLayer.
     */
    @Test
    public void testCreateOrder() throws Exception {
        Order order = new Order();
        order.setCustomerName("Brooks");
        order.setProductType("Carpet");
        order.setState("OH");
        order.setArea(new BigDecimal("100"));
        service.createOrder(order);
    }

    /**
     * Test of removeOrder method, of class FlooringServiceLayer.
     */
    @Test
    public void testRemoveOrder() throws Exception {
        LocalDate today = LocalDate.now();
        service.removeOrder(today, 1);
    }

    /**
     * Test of editOrder method, of class FlooringServiceLayer.
     */
    @Test
    public void testEditOrder() throws Exception {
        LocalDate today = LocalDate.now();
        Order order = service.findOrder(today, 1);
        order.setCustomerName("John");
        order.setProductType("Laminate");
        order.setState("OH");
        order.setArea(new BigDecimal("100"));
        service.editOrder(today, order);
    }

    /**
     * Test of findAllOrdersOnDate method, of class FlooringServiceLayer.
     */
    @Test
    public void testFindAllOrdersOnDate() throws Exception {
        LocalDate today = LocalDate.now();
        assertEquals(service.findAllOrdersOnDate(today).size(),1);
    }

    /**
     * Test of findOrder method, of class FlooringServiceLayer.
     */
    @Test
    public void testFindOrder() throws Exception {
        LocalDate today = LocalDate.now();
        service.findOrder(today, 1);
    }

}
