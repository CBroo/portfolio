/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.Order;
import Dto.Product;
import Dto.Taxes;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author crbxi
 */
public class ProductDaoFileImpl implements ProductDao {
    List<Product> products =  new ArrayList<>();
    String PRODUCT_FILE = "Products.txt";
    String PRODUCT_DELIMITER = ",";
    
    @Override
    public Product getProduct(String productType) throws FlooringDaoException{
        loadProducts();
        for (Product product : products) {
            if(product.getProduct().equals(productType)) {
                return product;
            }
        }
        return null;
    }
    
    private void loadProducts() throws FlooringDaoException{
        Scanner scanner;
        try {
            scanner = new Scanner(new BufferedReader(new FileReader(PRODUCT_FILE)));
        } catch (FileNotFoundException e) {
            throw new FlooringDaoException("-_- Could not load product data into memory.", e);
        }
        
        String currentLine;
        String[] currentTokens;
        String[] currentProductTokens;
        scanner.nextLine();
        while(scanner.hasNextLine()) {
            currentLine = scanner.nextLine();
            currentTokens = currentLine.split(PRODUCT_DELIMITER);
            Product currentProduct = new Product(currentTokens[0]);//Product Type
            currentProduct.setCostPerSqFt(new BigDecimal(currentTokens[1]));//Cost Per Sq Ft
            currentProduct.setLaborCostPerSqFt(new BigDecimal(currentTokens[2]));//Labor Cost Per Sq Ft
            products.add(currentProduct);
        }
        scanner.close();
    }    
}
