/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.Taxes;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author crbxi
 */
public class TaxesDaoFileImpl implements TaxesDao {
    List<Taxes> taxes = new ArrayList<>();
    String TAXES_FILE = "Taxes.txt";
    String TAXES_DELIMITER = ",";
    
    @Override
    public Taxes getTax(String state) throws FlooringDaoException{
        loadTaxes();
        for (Taxes tax : taxes) {
            if(tax.getState().equals(state)) {
                return tax;
            }
        }
        return null;
    }
    
    public void loadTaxes() throws FlooringDaoException {
        Scanner scanner;
        try {
            scanner = new Scanner(new BufferedReader(new FileReader(TAXES_FILE)));
        } catch (FileNotFoundException e) {
            throw new FlooringDaoException("-_- Could not load tax data into memory.", e);
        }
        
        String currentLine;
        String[] currentTokens;
        String[] currentTaxTokens;
        scanner.nextLine();
        while(scanner.hasNextLine()) {
            currentLine = scanner.nextLine();
            currentTokens = currentLine.split(TAXES_DELIMITER);
            Taxes currentTaxes = new Taxes(currentTokens[0]);//State
            currentTaxes.setTax(new BigDecimal(currentTokens[1]));//Tax Rate
            taxes.add(currentTaxes);
        }
        scanner.close();
    }
}
