/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.Order;
import Dto.Product;
import Dto.Taxes;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author crbxi
 */
public class OrderDaoInMemoryImpl implements OrderDao {

    HashMap<LocalDate, List<Order>> orders = new HashMap<>();
    String ORDERS_DELIMITER = ",";

    @Override
    public void createOrder(Order order) throws FlooringDaoException {
        LocalDate today = LocalDate.now();
        List<Order> orderList = new ArrayList<>();
        if (orders.containsKey(today)) {
            orderList = orders.get(today);
        }
        int orderNum = findMaxIdNum(today);
        order.setOrderNum(orderNum);
        orderList.add(order);
        orders.put(today, orderList);
    }

    @Override
    public void removeOrder(LocalDate orderDate, int orderNum) throws FlooringDaoException {
        if (!orders.containsKey(orderDate)) {
            loadOrders(orderDate);
        }

        for (Iterator<Order> it = orders.get(orderDate).iterator(); it.hasNext();) {
            Order order = it.next();
            if (order.getOrderNum() == orderNum) {
                it.remove();
            }
        }
    }

    @Override
    public void editOrder(LocalDate orderDate, Order order) throws FlooringDaoException {
        List<Order> orderList = orders.get(orderDate);
        for (Order orderInList : orderList) {
            if (orderInList.getOrderNum() == order.getOrderNum()) {
                orderList.set(orderList.indexOf(orderInList), order);
            }
        }
    }

    @Override
    public List<Order> findAllOrdersOnDate(LocalDate orderDate) throws FlooringDaoException {
        if (!orders.containsKey(orderDate)) {
            loadOrders(orderDate);
            return orders.get(orderDate);
        }
        return orders.get(orderDate);
    }

    @Override
    public Order findOrder(LocalDate orderDate, int orderNum) throws FlooringDaoException {
        Order foundOrder = null;
        if (!orders.containsKey(orderDate)) {
            loadOrders(orderDate);
        }
        for (Order orderInList : orders.get(orderDate)) {
            if (orderInList.getOrderNum() == orderNum) {
                foundOrder = orderInList;
            }
        }
        if (foundOrder == null) {
            throw new FlooringDaoException("Order does not exist.");
        }
        return foundOrder;
    }

    public int findMaxIdNum(LocalDate orderDate) {
        int orderNum;
        if (orders.containsKey(orderDate)) {
            List<Order> orderList = orders.get(orderDate);
            orderNum = orderList.get(orderList.size() - 1).getOrderNum() + 1;//finding last value in order list, adding 1 to the orderNum and returning
            if (orderNum == 1) {//If there is only 1 element in list, the above will end up replacing it
                orderNum = 2;//this is messy, need to clean up somehow
            }
        } else {
            orderNum = 1;
        }
        return orderNum;
    }

    private void loadOrders(LocalDate orderDate) throws FlooringDaoException {
        Scanner scanner;
        DateTimeFormatter format = DateTimeFormatter.ofPattern("MMddyyyy");
        String orderDateString = orderDate.format(format);
        try {
            scanner = new Scanner(new BufferedReader(new FileReader("Orders_" + orderDateString + ".txt")));
        } catch (FileNotFoundException e) {
            throw new FlooringDaoException("-_- Could not load order data into memory.", e);
        }

        String currentLine;
        String[] currentTokens;
        String[] currentOrderTokens;
        List<Order> orderList = new ArrayList<>();
        while (scanner.hasNextLine()) {
            currentLine = scanner.nextLine();
            currentTokens = currentLine.split(ORDERS_DELIMITER);
            Order currentOrder = new Order();
            currentOrder.setOrderNum(Integer.parseInt(currentTokens[0]));//OrderNum
            currentOrder.setCustomerName(currentTokens[1]);//CustomerName
            currentOrder.setState(currentTokens[2]);//State
            Taxes currentTaxes = new Taxes(currentOrder.getState());
            currentTaxes.setTax(new BigDecimal(currentTokens[3]));//taxRate
            currentOrder.setTaxes(currentTaxes);
            currentOrder.setProductType(currentTokens[4]);//productType
            Product currentProduct = new Product(currentOrder.getProductType());
            currentOrder.setArea(new BigDecimal(currentTokens[5]));//area
            currentProduct.setCostPerSqFt(new BigDecimal(currentTokens[6]));//costPerSqFt
            currentProduct.setLaborCostPerSqFt(new BigDecimal(currentTokens[7]));//laborCostPerSqFt
            currentProduct.setMaterialCost(new BigDecimal(currentTokens[8]));//materialCosts
            currentOrder.setProduct(currentProduct);
            currentOrder.setLaborCosts(new BigDecimal(currentTokens[9]));//laborCosts
            currentOrder.setTotalTax(new BigDecimal(currentTokens[10]));//totalTax
            currentOrder.setTotalCost(new BigDecimal(currentTokens[11]));//totalCosts

            orderList.add(currentOrder);
            orders.put(orderDate, orderList);
        }
        scanner.close();
    }

    private void writeOrders(LocalDate orderDate) throws FlooringDaoException {
        //nothing goes here :)
    }

    @Override
    public void save() throws FlooringDaoException {
        for (LocalDate dates : orders.keySet()) {
            writeOrders(dates);
        }

    }
}
