/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Dto.Order;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author crbxi
 */
public interface OrderDao {
    void createOrder(Order order) throws FlooringDaoException;
    void removeOrder(LocalDate orderDate, int orderNum) throws FlooringDaoException;
    void editOrder(LocalDate orderDate, Order order) throws FlooringDaoException;
    List<Order> findAllOrdersOnDate(LocalDate orderDate) throws FlooringDaoException;
    Order findOrder(LocalDate orderDate, int orderNum) throws FlooringDaoException, FlooringOrderNotFoundException;
    void save() throws FlooringDaoException;
}
