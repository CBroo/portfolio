/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Dao.FlooringAuditDao;
import Dao.FlooringDaoException;
import Dao.FlooringOrderNotFoundException;
import Dao.TaxesDao;
import Dto.Order;
import Dto.Product;
import Dto.Taxes;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import Dao.OrderDao;
import Dao.ProductDao;

/**
 *
 * @author crbxi
 */
public class FlooringServiceLayerImpl implements FlooringServiceLayer {

    OrderDao ordersDao;
    ProductDao productsDao;
    TaxesDao taxesDao;
    FlooringAuditDao auditDao;

    public FlooringServiceLayerImpl(OrderDao ordersDao, ProductDao productsDao, TaxesDao taxesDao, FlooringAuditDao auditDao) {
        this.ordersDao = ordersDao;
        this.productsDao = productsDao;
        this.taxesDao = taxesDao;
        this.auditDao = auditDao;
    }

    @Override
    public void createOrder(Order order) throws FlooringDaoException, FlooringDataValidationException {
        Product product = productsDao.getProduct(order.getProductType());
        Taxes taxes = taxesDao.getTax(order.getState());
        order.setProduct(product);
        order.setTaxes(taxes);
        order.getProduct().setMaterialCost(calculateMaterialCost(order));
        order.setLaborCosts(calculateLaborCost(order));
        order.setTotalTax(calculateTotalTax(order));
        order.setTotalCost(calculateTotalCost(order));
        try {
            validateData(order);
        } catch (FlooringDataValidationException e) {
            throw new FlooringDataValidationException("Invalid Data Present", e);
        }
        ordersDao.createOrder(order);
    }

    @Override
    public void removeOrder(LocalDate orderDate, int orderNum) throws FlooringDaoException {
        ordersDao.removeOrder(orderDate, orderNum);
    }

    @Override
    public void editOrder(LocalDate orderDate, Order order) throws FlooringDaoException {
        Product product = productsDao.getProduct(order.getProductType());
        Taxes taxes = taxesDao.getTax(order.getState());
        order.setProduct(product);
        order.setTaxes(taxes);
        order.getProduct().setMaterialCost(calculateMaterialCost(order));
        order.setLaborCosts(calculateLaborCost(order));
        order.setTotalTax(calculateTotalTax(order));
        order.setTotalCost(calculateTotalCost(order));
        ordersDao.editOrder(orderDate, order);
    }

    @Override
    public List<Order> findAllOrdersOnDate(LocalDate orderDate) throws FlooringDaoException, FlooringDataValidationException {
        List<Order> ordersOnDate = ordersDao.findAllOrdersOnDate(orderDate);
        for (Order order : ordersOnDate) {
            validateData(order);
        }
        return ordersOnDate;
    }

    @Override
    public Order findOrder(LocalDate orderDate, int orderNum) throws FlooringDaoException, FlooringDataValidationException, FlooringOrderNotFoundException {
        Order order;
        try {
            order = ordersDao.findOrder(orderDate, orderNum);
        } catch (FlooringOrderNotFoundException e) {
            throw new FlooringOrderNotFoundException("Order Not Found", e);
        };
        validateData(order);
        return order;
    }

    public BigDecimal calculateMaterialCost(Order order) {
        BigDecimal costPerSqFt = order.getProduct().getCostPerSqFt();
        BigDecimal materialCost = costPerSqFt.multiply(order.getArea()).setScale(2, BigDecimal.ROUND_HALF_UP);
        return materialCost;
    }

    public BigDecimal calculateLaborCost(Order order) {
        BigDecimal laborCostPerSqFt = order.getProduct().getLaborCostPerSqFt();
        BigDecimal laborCost = laborCostPerSqFt.multiply(order.getArea()).setScale(2, BigDecimal.ROUND_HALF_UP);
        return laborCost;
    }

    public BigDecimal calculateTotalTax(Order order) {
        BigDecimal materialCost = order.getProduct().getMaterialCost();
        BigDecimal laborCost = order.getLaborCosts();
        BigDecimal taxRate = order.getTaxes().getTax().multiply(new BigDecimal(.01));
        BigDecimal taxCost = (laborCost.add(materialCost)).multiply(taxRate).setScale(2, BigDecimal.ROUND_HALF_UP);
        return taxCost;
    }

    public BigDecimal calculateTotalCost(Order order) {
        BigDecimal materialCost = order.getProduct().getMaterialCost();
        BigDecimal laborCost = order.getLaborCosts();
        BigDecimal taxRate = order.getTaxes().getTax().multiply(new BigDecimal(.01));
        BigDecimal taxCost = (laborCost.add(materialCost)).multiply(taxRate);
        BigDecimal totalCost = laborCost.add(materialCost).add(taxCost).setScale(2, BigDecimal.ROUND_HALF_UP);
        return totalCost;
    }
    
    public void validateData(Order order) throws FlooringDataValidationException {
        try {
            validateOrderData(order);
            validateTaxData(order);
            validateProductData(order);
        } catch (FlooringDataValidationException e) {
            throw new FlooringDataValidationException("Invalid Data Present", e);
        }
    }

    public void validateOrderData(Order order) throws FlooringDataValidationException {
        if (order.getCustomerName() == null || order.getCustomerName().trim().length() == 0
                || order.getArea() == null || order.getArea().compareTo(BigDecimal.ZERO) < 0
                || order.getLaborCosts() == null || order.getLaborCosts().compareTo(BigDecimal.ZERO) < 0
                || order.getProductType() == null || order.getProductType().trim().length() == 0
                || order.getState() == null || order.getState().trim().length() == 0
                || order.getTotalCost() == null || order.getTotalCost().compareTo(BigDecimal.ZERO) < 0
                || order.getTotalTax() == null || order.getTotalTax().compareTo(BigDecimal.ZERO) < 0) {
            throw new FlooringDataValidationException("ERROR: Order Data Must Be Valid And Must Exist!!");
        }
    }

    public void validateTaxData(Order order) throws FlooringDataValidationException {
        if (order.getTaxes() == null
                || order.getTaxes().getState() == null || order.getTaxes().getState().trim().length() == 0
                || order.getTaxes().getTax() == null || order.getTaxes().getTax().compareTo(BigDecimal.ZERO) < 0) {
            throw new FlooringDataValidationException("ERROR: Tax Data Must Be Valid And Must Exist!!!");
        }
    }

    public void validateProductData(Order order) throws FlooringDataValidationException {
        if (order.getProduct() == null
                || order.getProduct().getCostPerSqFt() == null || order.getProduct().getCostPerSqFt().compareTo(BigDecimal.ZERO) < 0
                || order.getProduct().getLaborCostPerSqFt() == null || order.getProduct().getLaborCostPerSqFt().compareTo(BigDecimal.ZERO) < 0
                || order.getProduct().getMaterialCost() == null || order.getProduct().getMaterialCost().compareTo(BigDecimal.ZERO) < 0
                || order.getProduct().getProduct() == null || order.getProduct().getProduct().trim().length() == 0) {
            throw new FlooringDataValidationException("ERROR: Product Data Must Be Valid And Must Exist!!!");
        }
    }

    @Override
    public void save() throws FlooringDaoException {
        ordersDao.save();
    }
}
