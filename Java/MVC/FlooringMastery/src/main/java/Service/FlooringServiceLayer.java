/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Service;

import Dao.FlooringDaoException;
import Dao.FlooringOrderNotFoundException;
import Dto.Order;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author crbxi
 */
public interface FlooringServiceLayer {
    void createOrder(Order order) throws FlooringDaoException, FlooringDataValidationException;
    void removeOrder(LocalDate orderDate, int orderNum) throws FlooringDaoException;
    void editOrder(LocalDate orderDate, Order order) throws FlooringDaoException;
    List<Order> findAllOrdersOnDate(LocalDate orderDate) throws FlooringDaoException, FlooringDataValidationException;
    Order findOrder(LocalDate orderDate, int orderNum) throws FlooringDaoException, FlooringDataValidationException, FlooringOrderNotFoundException;
    void save() throws FlooringDaoException;
}
