/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Dto.Order;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author crbxi
 */
public class FlooringView {

    private UserIO io;

    public FlooringView(UserIO io) {
        this.io = io;
    }

    public int printMenuAndGetSelection() {
        io.print("-Main Menu-");
        io.print("1. Display Orders");
        io.print("2. Add An Order");
        io.print("3. Edit An Order");
        io.print("4. Remove An Order");
        io.print("5. Save Current Work");
        io.print("6. Exit");

        return io.readInt("Please select from the above choices.", 1, 6);
    }

    public void displayOrdersOnDate(List<Order> ordersOnDate) {
        for (Order order : ordersOnDate) {
            io.print("Order Number: " + order.getOrderNum());
            io.print("Customer Name: " + order.getCustomerName());
            io.print("Product: " + order.getProductType());
            io.print("State: " + order.getState());
            io.print("State Tax Rate: %" + order.getTaxes().getTax());
            io.print("Area: " + order.getArea());
            io.print("Labor Cost Per Square Foot: $" + order.getProduct().getLaborCostPerSqFt());
            io.print("Cost Per Square Foot: $" + order.getProduct().getCostPerSqFt());
            io.print("Material Cost: $" + order.getProduct().getMaterialCost());
            io.print("Total Labor Costs: $" + order.getLaborCosts());
            io.print("Total Tax: $" + order.getTotalTax());
            io.print("Total Costs: $" + order.getTotalCost());
            io.print("");
        }
        io.readString("Please hit enter to continue.");
    }

    public LocalDate getOrderDateChoice() {
        LocalDate orderDateChoice = io.readLocalDate("Please Enter The Date of the Order in the Following Format MM-dd-yyyy:");
        return orderDateChoice;
    }

    public int getOrderNumChoice() {
        int orderNum = io.readInt("Please Enter The Order Number:");
        return orderNum;
    }

    public Order createOrder(Order order) {
        order.setCustomerName(io.readString("Enter Customer's Last Name: "));
        order.setProductType(getProductChoice());
        order.setState(getStateChoice());
        order.setArea(areaChoice());
        return order;
    }

    public String getProductChoice() {
        io.print("1. Carpet");
        io.print("2. Laminate");
        io.print("3. Tile");
        io.print("4. Wood");
        String productName = "";
        int choice = io.readInt("Please select from the above product choices.", 1, 4);
        switch (choice) {
            case 1:
                productName = "Carpet";
            case 2:
                productName = "Laminate";
            case 3:
                productName = "Tile";
            case 4:
                productName = "Wood";
        }
        return productName;
    }

    public String getStateChoice() {
        io.print("1. Ohio");
        io.print("2. Pennsylvania");
        io.print("3. Michigan");
        io.print("4. Indiana");
        String productName = "";
        int choice = io.readInt("Please select from the above state choices.", 1, 4);
        switch (choice) {
            case 1:
                productName = "OH";
            case 2:
                productName = "PA";
            case 3:
                productName = "MI";
            case 4:
                productName = "IN";
        }
        return productName;
    }

    public BigDecimal areaChoice() {
        String choice = "";
        BigDecimal test;
        while (true) {
            try {
                choice = io.readString("Enter Area of Order in Square Feet: ");
                test = new BigDecimal(choice);
                break;
            } catch (NumberFormatException e) {
                io.print("You must enter a valid number.");
            }
        }
        return test;
    }

    public Order editOrder(Order order) {
        String edit = "";
        BigDecimal editBD;
        edit = io.readString("Current Last Name is: " + order.getCustomerName() + "\nWhat would you like to change it to? ");
        if (!edit.equals("")) {
            order.setCustomerName(edit);
        }

        io.print("Current Product Type is: " + order.getProductType() + "\nWhat would you like to change it to? ");
        edit = getProductChoice();
        if (!edit.equals("")) {
            order.setProductType(edit);
        }

        io.print("Current State is: " + order.getState() + "\nWhat would you like to change it to? ");
        edit = getStateChoice();
        if (!edit.equals("")) {
            order.setState(edit);
        }

        io.print("Current Area is: " + order.getArea() + "\nWhat would you like to change it to? ");
        editBD = areaChoice();
        if (!edit.equals("")) {
            order.setArea(editBD);
        }

        return order;
    }
    
    public void displayAllOrdersBanner() {
        io.print("---Displaying Orders---");
    }
    
    public void displayOrderCreatedBanner() {
        io.print("---Order Created---");
    }
    
    public void displayOrderEditBanner() {
        io.print("---Order Edited---");
    }
    
    public void displayOrderRemovedBanner() {
        io.print("---Order Removed---");
    }
    
    public void displaySaveBanner() {
        io.print("---Information Saved To File---");
    }

    public void displayErrorMessage(String errorMsg) {
        io.print("===ERROR===");
        io.print(errorMsg);
    }

    public void displayExitBanner() {
        io.print("Thank You! Good Bye!");
    }

    public void displayUnknownCommandBanner() {
        io.print("Unknown Command!!!");
    }
}
