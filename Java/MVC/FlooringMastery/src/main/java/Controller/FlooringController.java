/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.FlooringDaoException;
import Dao.FlooringOrderNotFoundException;
import Dto.Order;
import Service.FlooringDataValidationException;
import Service.FlooringServiceLayer;
import UI.FlooringView;
import java.time.LocalDate;
import java.util.List;

/**
 *
 * @author crbxi
 */
public class FlooringController {

    FlooringView view;
    private FlooringServiceLayer service;

    public FlooringController(FlooringServiceLayer service, FlooringView view) {
        this.service = service;
        this.view = view;
    }

    public void run() {
        boolean keepGoing = true;
        int menuSelection = 0;
        while (keepGoing) {
            try {
                menuSelection = getMenuSelection();

                switch (menuSelection) {
                    case 1:
                        listOrder();
                        break;
                    case 2:
                        addOrder();
                        break;
                    case 3:
                        editOrder();
                        break;
                    case 4:
                        removeOrder();
                        break;
                    case 5:
                        save();
                        break;
                    case 6:
                        keepGoing = false;
                        break;
                    default:
                        unknownCommand();
                }
            } catch (FlooringDaoException | FlooringDataValidationException | FlooringOrderNotFoundException e ) {
                view.displayErrorMessage(e.getMessage());
            }
        }
        exitMessage();
    }

    private int getMenuSelection() {
        return view.printMenuAndGetSelection();
    }

    private void listOrder() throws FlooringDaoException, FlooringDataValidationException {
        LocalDate orderDateChoice = view.getOrderDateChoice();
        List<Order> ordersOnDate = service.findAllOrdersOnDate(orderDateChoice);
        view.displayAllOrdersBanner();
        view.displayOrdersOnDate(ordersOnDate);
    }

    private void addOrder() throws FlooringDaoException, FlooringDataValidationException {
        Order order = new Order();
        view.createOrder(order);
        service.createOrder(order);
        view.displayOrderCreatedBanner();
    }

    private void editOrder() throws FlooringDaoException, FlooringDataValidationException, FlooringOrderNotFoundException {
        LocalDate orderDateChoice = view.getOrderDateChoice();
        int orderNum = view.getOrderNumChoice();
        Order currentOrder = service.findOrder(orderDateChoice, orderNum);
        currentOrder = view.editOrder(currentOrder);
        service.editOrder(orderDateChoice, currentOrder);
        view.displayOrderEditBanner();
    }

    private void removeOrder() throws FlooringDaoException {
        LocalDate orderDateChoice = view.getOrderDateChoice();
        int orderNum = view.getOrderNumChoice();
        service.removeOrder(orderDateChoice, orderNum);
        view.displayOrderRemovedBanner();
    }

    private void save() throws FlooringDaoException {
        service.save();
        view.displaySaveBanner();
    }
    

    private void exitMessage() {
        view.displayExitBanner();
    }

    private void unknownCommand() {
        view.displayUnknownCommandBanner();
    }
}
