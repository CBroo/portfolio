/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dto;

import java.math.BigDecimal;

/**
 *
 * @author crbxi
 */
public class Taxes {
    BigDecimal tax;
    String state;
    
    public Taxes(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public BigDecimal getTax() {
        return tax;
    }

    public void setTax(BigDecimal tax) {
        this.tax = tax;
    }
    
}
