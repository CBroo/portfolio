This porfolio contains some of my work. These projects represent a overview of the things I have worked with, not the extent of my knowledge about certain languages or frameworks. 
I am also currently enrolled in a program where I am actively coding 8+ hours a day so many of these projects are not polished and will be updated frequently.

If you have any questions, you can contact me at ContactCBrooks@gmail.com.